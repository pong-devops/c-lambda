import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.kafka010._

object LogProcessor {
  def main(args: Array[String]): Unit = {
    // Khởi tạo SparkSession
    val spark = SparkSession.builder()
      .appName("LogDataProcessing")
      .getOrCreate()

    // Định nghĩa cấu trúc của DataFrame
    val schema = StructType(Seq(
      StructField("userId", StringType, nullable = true),
      StructField("action", StringType, nullable = true),
      StructField("content", StringType, nullable = true),
      StructField("time", StringType, nullable = true),
      StructField("ip", StringType, nullable = true)
    ))

    // Đọc dữ liệu từ HDFS và áp dụng schema
    val logData = spark.readStream
      .option("delimiter", "|")
      .schema(schema)
      .csv("hdfs://hadoop-master.citics.vn:9000/opt/logs/flume_out/agent1")

    // Xử lý dữ liệu
    val processedData = logData
      .withColumn("action", lit("citics_new_stream"))
      .select("userId", "action", "content", "time", "ip")
      .withColumn("value", to_json(struct("userId", "action", "content", "time", "ip"))) // Sử dụng hàm struct để tạo JSON

    // Gửi dữ liệu đã xử lý vào Kafka topic
    val query = processedData.writeStream
      .outputMode("append")
      .format("kafka")
      .option("kafka.bootstrap.servers", "apache-kafka.citics.vn:9092")
      .option("topic", "c-customers-stream")
      .option("checkpointLocation", "/opt/logs/checkpoint/dir")
      .start()

    query.awaitTermination()

    // Đóng SparkSession
    spark.stop()
  }
}
