cd $SPARK_HOME/src/main/scala/
sbt package
spark-submit \
  --class LogProcessor \
  --master spark://apache-spark-stream:7077 \
  --packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.5.1 \
  target/scala-2.12/lambda-spark-stream_2.12-1.0.jar
