# Use ubuntu 22.04 as the base image
FROM ubuntu:22.04
LABEL maintainer="Phong Dao <phong.daodh@gmail.com>"
# Define Hadoop version as a build argument
ARG SPARK_STREAM_VERSION=1.0.0

WORKDIR /root

# Install necessary packages
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    wget \
    openjdk-8-jdk \
    apt-transport-https \
    curl \
    gnupg && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Add apt repository of sbt
RUN echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list && \
    echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list && \
    curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/scalasbt-release.gpg --import && \
    chmod 644 /etc/apt/trusted.gpg.d/scalasbt-release.gpg && \
    apt-get update && \
    apt-get install -y sbt

# Download and install Apache Spark
RUN wget https://dlcdn.apache.org/spark/spark-3.5.1/spark-3.5.1-bin-hadoop3.tgz && \
    tar xvzf spark-3.5.1-bin-hadoop3.tgz && \
    mv spark-3.5.1-bin-hadoop3 /usr/local/apache-spark-stream && \
    rm spark-3.5.1-bin-hadoop3.tgz

# Set environment variables
ENV SPARK_HOME=/usr/local/apache-spark-stream
ENV PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin

# Add script process data
COPY script/process_data.sh $SPARK_HOME/script/process_data.sh
RUN chmod +x $SPARK_HOME/script/process_data.sh

# Create Scala for build and process data
#RUN mkdir -p /usr/local/apache-spark/src/main/scala/
COPY config/LogProcessor.scala $SPARK_HOME/src/main/scala/LogProcessor.scala
COPY config/build.sbt $SPARK_HOME/src/main/scala/build.sbt

# docker build --build-arg SPARK_STREAM_VERSION=1.0.0 -t phongddh76/a-lambda-spark-stream:1.0.0 .
# docker login
# docker push phongddh76/a-lambda-spark-stream:1.0.0