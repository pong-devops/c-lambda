#!/bin/bash

# Xoá container
docker-compose down

# Xoá mạng
docker network prune -f

# Kiểm tra xem có images nào tồn tại không trước khi xóa
if [[ $(docker images -q) ]]; then
    # Nếu có, thì xóa
    docker rmi -f $(docker images -q)
else
    echo "Không có images nào để xóa."
fi