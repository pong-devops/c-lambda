README.md

### A. Get Project
##### 1. Install Docker and Docker Compose
```
$ curl -fsSL https://get.docker.com/ | sh
$ sudo usermod -aG docker $(whoami)

```
Or
```
$ curl -fsSL https://get.docker.com -o get-docker.sh
$ sudo sh ./get-docker.sh --dry-run
$ sudo usermod -aG docker $(whoami)
```
```
$ DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
$ mkdir -p $DOCKER_CONFIG/cli-plugins
$ curl -SL https://github.com/docker/compose/releases/download/v2.26.0/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose
$ sudo chmod +x /usr/local/lib/docker/cli-plugins/docker-compose
$ docker version
$ docker compose version
```

##### 2. Clone Project from Gitlab
```
$ git clone https://gitlab.com/pong-devops/c-lambda.git
```

##### 3. Run Docker Compose to start Service Hadoop
```
$ cd c-lambda
$ chmod +x fresh_script.sh
$ bash fresh_script.sh && docker-compose up -d
```
Web UI
```
http://yourdomain.com:9870/
```

##### 4. View logs, data
##### 4.1. Flume
Input logs to Flume
```
$ docker exec -it apache-flume /bin/bash
$ echo 'userId5|action5|content5|time5|ip5|token5' >> /opt/logs/agent1/agent1_error.log
```

Accesss Web UI and view log in /opt/logs/flume_out/agent1 for input logs

##### 4.2 Spark Batch
Accesss Web UI and view batch log in /opt/logs/spark/data-batch-processed for batch log processed
Accesss Web UI and view batch log in /opt/logs/hive/data-batch-processed for batch log write to hive with sql command
View logs processed in hive
```
$ docker exec -it hadoop-master.yourdomain.com /bin/bash
$ $HIVE_HOME/bin/beeline -n hadoop -u jdbc:hive2://localhost:10000
$ show databases;
$ SELECT * FROM lambda_database_batch_log.c_customers;
```

##### 4.2 Spark Stream
Witch Stream we can view logs in Kafka
```
docker exec -it apache-kafka.yourdomain.com /bin/bash
cd /usr/local/apache-kafka
bin/kafka-console-consumer.sh --bootstrap-server apache-kafka:9092 --topic c-customers-stream --from-beginning
```

##### 4. Important
```
Firewall all ports in HADOOP.
```

### B. References
```
http://odewahn.github.io/docker-jumpstart/building-images-with-dockerfiles.html
https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-common/ClusterSetup.html
https://hadoop.apache.org/docs/current/hadoop-project-dist/hadoop-hdfs/HdfsDesign.html
https://github.com/rahulkadamid/Lambda-Architecture-Data-Pipeline
https://dataguyin.medium.com/flume-spark-streaming-integration-8f6dfe67e502
```
