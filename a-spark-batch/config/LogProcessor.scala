import org.apache.spark.sql.{SparkSession, DataFrame}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import java.nio.file.{Paths, Files}
import java.nio.charset.StandardCharsets

object LogProcessor {
  // Đường dẫn đến file trạng thái
  val stateFilePath = "/usr/local/apache-spark-batch/src/main/scala/statefile.txt"

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder()
      .appName("LogDataProcessing")
      .getOrCreate()
    
    // Import implicits
    import spark.implicits._

    // Định nghĩa cấu trúc của DataFrame
    val schema = StructType(Seq(
      StructField("userId", StringType, nullable = true),
      StructField("action", StringType, nullable = true),
      StructField("content", StringType, nullable = true),
      StructField("time", StringType, nullable = true),
      StructField("ip", StringType, nullable = true),
      StructField("token", StringType, nullable = true)
    ))

    // Đọc trạng thái lần xử lý cuối cùng từ file trạng thái
    val lastProcessedTime = readLastProcessedTime()

    // Đọc dữ liệu từ HDFS và áp dụng schema
    val logData = spark.read.schema(schema).option("delimiter", "|")
      .csv("hdfs://hadoop-master.citics.vn:9000/opt/logs/flume_out/agent1")
      .filter($"time" > lastProcessedTime) // Lọc dữ liệu mới

    // Xử lý dữ liệu
    val processedData = logData.withColumn("action", lit("citics-batch")).drop("token")

    // Lưu kết quả xử lý trở lại HDFS
    processedData.write.mode("append").option("delimiter", "|")
      .csv("hdfs://hadoop-master.citics.vn:9000/opt/logs/spark/data-batch-processed")

    // Cập nhật trạng thái lần xử lý cuối cùng
    updateLastProcessedTime(processedData)

    spark.stop()
  }

  def readLastProcessedTime(): String = {
    // Kiểm tra xem file trạng thái có tồn tại không
    if (Files.exists(Paths.get(stateFilePath))) {
      // Đọc dữ liệu từ file
      new String(Files.readAllBytes(Paths.get(stateFilePath)), StandardCharsets.UTF_8)
    } else {
      // Nếu file không tồn tại, trả về một giá trị mặc định
      "1970-01-01T00:00:00Z"
    }
  }

  def updateLastProcessedTime(df: DataFrame): Unit = {
    // Kiểm tra xem DataFrame có dữ liệu không
    if (!df.isEmpty) {
      // Lấy thời gian mới nhất từ DataFrame
      val maxTime = df.agg(max("time")).head.getString(0)

      // Ghi thời gian mới nhất vào file trạng thái
      Files.write(Paths.get(stateFilePath), maxTime.getBytes(StandardCharsets.UTF_8))
    }
  }
}
