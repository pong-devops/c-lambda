cd $SPARK_HOME/src/main/scala/
sbt package
#spark-submit --class LogProcessor --master spark://apache-spark-stream:7077 --packages org.apache.kafka:kafka-clients:3.6.1 target/scala-2.12/lambda-spark-stream_2.12-1.0.jar
spark-submit --class LogProcessor --master spark://apache-spark-batch:7077 target/scala-2.12/lambda-spark-batch_2.12-1.0.jar
