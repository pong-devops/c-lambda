#!/bin/bash

# Đặt biến môi trường
export SPARK_HOME=/usr/local/apache-spark-batch
export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin

# Di chuyển đến thư mục chứa mã nguồn Scala
cd $SPARK_HOME/src/main/scala/
#sbt package
# Sử dụng spark-submit để chạy ứng dụng Scala
spark-submit --class LogProcessor --master spark://apache-spark-batch:7077 target/scala-2.12/lambda-spark-batch_2.12-1.0.jar
