# Use ubuntu 22.04 as the base image
FROM ubuntu:22.04
LABEL maintainer="Phong Dao <phong.daodh@gmail.com>"
# Define Hadoop version as a build argument
ARG HADOOP_VERSION=1.0.0

WORKDIR /root

# Install necessary packages and create hadoop user
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    wget \
    openjdk-8-jdk \
    openssh-server

# Download and install Apache Hadoop
RUN wget https://dlcdn.apache.org/hadoop/common/hadoop-3.3.6/hadoop-3.3.6.tar.gz && \
    tar -xvzf hadoop-3.3.6.tar.gz && \
    mv hadoop-3.3.6 /usr/local/hadoop && \
    rm hadoop-3.3.6.tar.gz

# Set environment variables
ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV HADOOP_HOME=/usr/local/hadoop
ENV HADOOP_INSTALL=$HADOOP_HOME
ENV HDFS_DATANODE_USER=root
ENV HDFS_SECONDARYNAMENODE_USER=root
ENV YARN_RESOURCEMANAGER_USER=root
ENV YARN_NODEMANAGER_USER=root
ENV HADOOP_MAPRED_HOME=$HADOOP_HOME
ENV HADOOP_COMMON_HOME=$HADOOP_HOME
ENV HADOOP_HDFS_HOME=$HADOOP_HOME
ENV HADOOP_YARN_HOME=$HADOOP_HOME
ENV HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
ENV PATH=$PATH:$HADOOP_HOME/sbin:$HADOOP_HOME/bin
ENV HADOOP_OPTS="-Djava.library.path=$HADOOP_HOME/lib/native"

# Define the default number of Hadoop slaves in the cluster, which can be changed in Docker Compose
ENV HADOOP_SLAVE_NUMBER 2

# SSH without key
RUN ssh-keygen -t rsa -f /root/.ssh/id_rsa -P '' && \
    cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys

# Create default directories
RUN mkdir -p /root/hdfs/namenode && \
    mkdir -p /root/hdfs/datanode && \
    mkdir -p /root/hdfs/hadooptmp && \
    mkdir $HADOOP_HOME/logs

# Copy resources from host to container
ADD config/hadoop/ssh_config /root/.ssh/config
ADD config/hadoop/hadoop-env.sh $HADOOP_HOME/etc/hadoop/hadoop-env.sh
ADD config/hadoop/hdfs-site.xml $HADOOP_HOME/etc/hadoop/hdfs-site.xml
ADD config/hadoop/core-site.xml $HADOOP_HOME/etc/hadoop/core-site.xml
ADD config/hadoop/mapred-site.xml $HADOOP_HOME/etc/hadoop/mapred-site.xml
ADD config/hadoop/yarn-site.xml $HADOOP_HOME/etc/hadoop/yarn-site.xml
ADD config/hadoop/slaves $HADOOP_HOME/etc/hadoop/slaves
ADD script/hadoop/start_hadoop.sh /root/start_hadoop.sh
ADD script/hadoop/run_wordcount.sh /root/run_wordcount.sh

RUN chmod +x /root/start_hadoop.sh && \
    chmod +x /root/run_wordcount.sh && \
    chmod +x $HADOOP_HOME/etc/hadoop/slaves && \
    chmod +x $HADOOP_HOME/sbin/start-dfs.sh && \
    chmod +x $HADOOP_HOME/sbin/start-yarn.sh

# Format namenode
RUN $HADOOP_HOME/bin/hdfs namenode -format
# Expose HDFS ports
# EXPOSE 50010 50020 50070 50075 50090 8020 9000
# Expose Mapred ports
# EXPOSE 10020 19888
# Expose Yarn ports
# EXPOSE 8030 8031 8032 8033 8040 8042 8088
# docker build --build-arg HADOOP_VERSION=1.0.0 -t phongddh76/a-lambda-hadoop:1.0.0 . --no-cache
# docker login
# docker push phongddh76/a-lambda-hadoop:1.0.0