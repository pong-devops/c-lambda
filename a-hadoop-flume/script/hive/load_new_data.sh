#!/bin/bash

# Thiết lập biến môi trường
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export HADOOP_HOME=/usr/local/hadoop
export HADOOP_INSTALL=$HADOOP_HOME
export HDFS_DATANODE_USER=root
export HDFS_SECONDARYNAMENODE_USER=root
export YARN_RESOURCEMANAGER_USER=root
export YARN_NODEMANAGER_USER=root
export HADOOP_MAPRED_HOME=$HADOOP_HOME
export HADOOP_COMMON_HOME=$HADOOP_HOME
export HADOOP_HDFS_HOME=$HADOOP_HOME
export HADOOP_YARN_HOME=$HADOOP_HOME
export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
export PATH=$PATH:$HADOOP_HOME/sbin:$HADOOP_HOME/bin
export HIVE_HOME=/usr/local/apache-hive
export PATH=$PATH:$HIVE_HOME/bin
export HADOOP_OPTS="-Djava.library.path=$HADOOP_HOME/lib/native"

# Hàm kiểm tra xem tệp dữ liệu trên HDFS đã tồn tại và có dữ liệu hay không
check_hdfs_data() {
    # Lấy danh sách tất cả các tệp trong thư mục
    hdfs_files=$(hadoop fs -ls hdfs://hadoop-master.citics.vn:9000/opt/logs/spark/data-batch-processed | awk '{print $NF}')
    
    # Kiểm tra xem có tệp nào không
    if [ -z "$hdfs_files" ]; then
        return 1 # Không có tệp
    else
        return 0 # Có tệp
    fi
}

# Chờ cho đến khi có dữ liệu trên HDFS
echo "Đang chờ dữ liệu trên HDFS..."
while ! check_hdfs_data; do
    sleep 1
done

# Tải dữ liệu vào bảng
$HIVE_HOME/bin/beeline -n hadoop -u jdbc:hive2://localhost:10000 -e "USE lambda_database_batch_log; LOAD DATA INPATH 'hdfs://hadoop-master.citics.vn:9000/opt/logs/spark/data-batch-processed' INTO TABLE c_customers;" &
