#Create tmp Directory
hdfs dfs -mkdir /tmp
hdfs dfs -chmod 777 /tmp

#Create warehouse Directory
hdfs dfs -mkdir -p /user/hive/warehouse
hdfs dfs -chmod g+w /user/hive/warehouse

#Create Directory Spark Processed
hadoop fs -mkdir -p /opt/logs/spark/data-batch-processed
#hadoop fs -mkdir -p /opt/logs/spark/data-stream-processed

#Create Directory Hive Processed and Storage
hadoop fs -mkdir -p /opt/logs/hive/data-batch-processed
#hadoop fs -mkdir -p /opt/logs/hive/data-stream-processed

#Add HADOOP HOME to hive config
echo 'export HADOOP_HOME=$HADOOP_HOME' >> $HIVE_HOME/bin/hive-config.sh