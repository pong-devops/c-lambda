#Initiate Derby Database
$HIVE_HOME/bin/schematool -dbType derby -initSchema &&
#Launching Hive
$HIVE_HOME/bin/hiveserver2 &

# Chờ cho Hive Server khởi động hoàn tất
echo "Waiting for Hive Server to start..."
while ! netstat -ntpl | grep ':10000 ' >/dev/null; do
    sleep 1
done


#Connect tới hive server
#$HIVE_HOME/bin/beeline -n hadoop -u jdbc:hive2://localhost:10000 &

#Create database and table
$HIVE_HOME/bin/beeline -n hadoop -u jdbc:hive2://localhost:10000 -f $HIVE_HOME/queries/table_batch_creation.hql &&
#$HIVE_HOME/bin/beeline -n hadoop -u jdbc:hive2://localhost:10000 -f $HIVE_HOME/queries/table_stream_creation.hql &&

# Hàm kiểm tra xem tệp dữ liệu trên HDFS đã tồn tại và có dữ liệu hay không
check_hdfs_data() {
    # Lấy danh sách tất cả các tệp trong thư mục
    hdfs_files=$(hadoop fs -ls hdfs://hadoop-master.citics.vn:9000/opt/logs/spark/data-batch-processed | awk '{print $NF}')
    
    # Kiểm tra xem có tệp nào không
    if [ -z "$hdfs_files" ]; then
        return 1 # Không có tệp
    else
        return 0 # Có tệp
    fi
}

# Chờ cho đến khi có dữ liệu trên HDFS
echo "Waiting for data on HDFS..."
while ! check_hdfs_data; do
    sleep 1
done

#Load data into table
$HIVE_HOME/bin/beeline -n hadoop -u jdbc:hive2://localhost:10000 -e "USE lambda_database_batch_log; LOAD DATA INPATH 'hdfs://hadoop-master.citics.vn:9000/opt/logs/spark/data-batch-processed' INTO TABLE c_customers;" &

#View ket qua
# 1. docker exec -it hadoop-master.citics.vn /bin/bash
# 2. $HIVE_HOME/bin/beeline -n hadoop -u jdbc:hive2://localhost:10000
# 3. show databases;
# 3. SELECT * FROM lambda_database_batch_log.c_customers;
# 3. SELECT * FROM lambda_database_stream_log.c_customers;
