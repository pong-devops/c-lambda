create database if not exists lambda_database_batch_log;
DROP TABLE IF EXISTS lambda_database_batch_log.c_customers;
DROP VIEW IF EXISTS lambda_database_batch_log.v_html_access_log;
use lambda_database_batch_log;

create external table c_customers (
	userId string,
	citics_new string,
	content string,
	event_time string,
	ip string
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '|'
LOCATION '/opt/logs/hive/data-batch-processed';

create view v_html_access_log as
	select * from c_customers where 0 <> instr(userId, '.html');