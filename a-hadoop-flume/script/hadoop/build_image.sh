#!/bin/bash

echo -e "\nCreate network hadoop ...\n"
docker network create hadoop

echo -e "\nBuilding Hadoop 3.3.6 cluster docker image...\n"
sudo docker build -t c_lambda_hadoop .
#sudo docker build -t seedotech/hadoop:2.9.2 .