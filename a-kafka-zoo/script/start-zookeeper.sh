#!/bin/bash

# Start Zookeeper
/usr/local/apache-kafka/bin/zookeeper-server-start.sh /usr/local/apache-kafka/config/zookeeper.properties &

# Keep container running
#tail -f /dev/null
